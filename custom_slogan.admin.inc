<?php
/**
 * @file
 * The custom slogan admin page,
 *
 */

function custom_slogan_admin_settings() {
  $showfield_form_element = array('#type' => 'checkbox', );
  $pattern_form_element = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#maxlength' => 256,
  );
  $form['patterns'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Slogan Pattern'),
    '#collapsible' => TRUE,
    '#theme' => 'custom_slogan_admin_settings',
  );
  $form['patterns']['scope'] = array(
    'custom_slogan_default'  =>  array('#type'  =>  'markup', '#value' => t('Global'), ),
    'custom_slogan_front'  =>  array('#type'  =>  'markup', '#value' => t('Global'), ),
  );
  //Default custom slogan values for front and global
  $form['patterns']['pattern'] = array(
    'custom_slogan_default' =>  array(
    '#title'  =>  t('Default'),
    '#default_value'  =>  variable_get('custom_slogan_default', variable_get('site_slogan', '[site-slogan]')),
    '#required' =>  TRUE,
    ) + $pattern_form_element,
    'custom_slogan_front' =>  array(
    '#title' => t('Front'),
    '#default_value' =>  variable_get('custom_slogan_front', variable_get('site_slogan', '[site-slogan]')),
    ) + $pattern_form_element,
  );
  //Custom slogan values for node types with checkbox to enable it
  $types = node_get_types();
  foreach ($types as $type) {
    $key = 'custom_slogan_type_'. $type->type;
    $form['patterns']['pattern'][$key] = array(
      '#title' => t('Content Type - %type', array('%type'  =>  $type->name)),
      '#default_value'  =>  variable_get($key, ''),
    ) + $pattern_form_element;
    $form['patterns']['showfield'][$key .'_showfield'] = array(
      '#default_value'  =>  variable_get($key .'_showfield', 0),
    ) + $showfield_form_element;
    $form['patterns']['scope'][$key] = array(
      '#type' => 'markup',
      '#value' =>  t('node'),
    );
  }
  //Custom slogan values for taxonomy vocabularies with checkbox to enable it
  if (module_exists('taxonomy')) {
  $vocabs = taxonomy_get_vocabularies();
  foreach ($vocabs as $vocab) {
    $key = 'custom_slogan_vocab_'. $vocab->vid;

    $form['patterns']['pattern'][$key] = array(
      '#title'  =>  t('Vocabulary - %vocab_name', array('%vocab_name'  =>  $vocab->name)),
      '#default_value'  =>  variable_get($key, ''),
    ) + $pattern_form_element;
    $form['patterns']['showfield'][$key .'_showfield'] = array(
      '#default_value'  =>  variable_get($key .'_showfield', 0),
    ) + $showfield_form_element;

    $form['patterns']['scope'][$key] = array(
      '#type' => 'markup',
      '#value' =>  t('Taxonomy'),
    );
   }
  }
  // Add the token help to a collapsed fieldset at the end of the configuration page.
  $form['token_help'] = array(
    '#type'  =>  'fieldset',
    '#title'  =>  t('Available Tokens List'),
    '#collapsible'  =>  TRUE,
    '#collapsed'  =>  TRUE,
  );
  $form['token_help']['content'] = array(
    '#type'  =>  'markup',
    '#value'  =>  theme('token_help'),
  );
  return system_settings_form($form);
}
/*
 * Implementation of theme().
 */

function theme_custom_slogan_admin_settings($form) {
  $rows = array();
  foreach (element_children($form['pattern']) as $key) {
    $title = array(
      '#type' => 'item',
      '#title' => $form['pattern'][$key]['#title'],
      '#required' => $form['pattern'][$key]['#required'],
    );
    unset($form['pattern'][$key]['#title']);
    $row = array();
    $row[] = drupal_render($title);
    $row[] = drupal_render($form['scope'][$key]);
    $row[] = drupal_render($form['pattern'][$key]);
    $row[] = drupal_render($form['showfield'][$key .'_showfield']);
    $rows[] = array('data' => $row, );
  }
  $header = array(t('Page Type'), t('Token Scope'), t('Pattern'),  t('Enabled'), );
  $form['#children'] = theme('table', $header, $rows, array('id' => 'taxonomy'));
  return drupal_render($form);
}